$('#confirmacaoExclusaoModal').on(
		'show.bs.modal',
		function(event) {

			var button = $(event.relatedTarget);

			var codigoTitulo = button.data('codigo');
			var descricaoTitulo = button.data('descricao');

			var modal = $(this);
			var formObject = modal.find('form');
			/*
			 * aqui é buscado o componente 'form' dentro do 'modal'
			 */

			var actionValue = formObject.data('url-base');
			/*
			 * aqui é buscado o valor contido no 'action' do 'form' encontrado
			 * dentro do 'modal'
			 */

			if (!actionValue.endsWith('/')) {
				actionValue += '/'; // adiciona '/' caso não exista
			}

			formObject.attr('action', actionValue + codigoTitulo);
			/*
			 * aqui ocorre a troca do conteúdo do action original pelo conteúdo
			 * do actionValue concatenado com o código do objeto que disparou o
			 * evento de exlusão
			 */

			modal.find('.modal-body span').html(
					'Tem certeza que deseja excluir o título <strong>'
							+ descricaoTitulo + '</strong> ?');
		});

$(function() {
	$('[rel="tooltip"]').tooltip();
	$('.input-group.date').datepicker({
		language : "pt-BR",
		todayHighlight : true
	});

	$('.js-value').maskMoney({
		symbol : 'R$ ',
		thousands : '.',
		decimal : ',',
		symbolStay : true
	});

	$('.js-atualizar-status').on(
			'click',
			function(event) {
				event.preventDefault();

				var buttonReceber = $(event.currentTarget);
				var urlReceber = buttonReceber.attr('href');

				var response = $.ajax({
					url : urlReceber,
					type : 'PUT'
				});

				response.done(function(e) {
					var codigoTitulo = buttonReceber.data('codigo');
					if(e == 'Recebido') {
						$('[data-role=' + codigoTitulo + '] span')
							.removeClass('label-warning')
							.addClass('label-success')
							.html(e);
					
						$('[data-codigo=' + codigoTitulo + '] i')
							.removeClass('fa-square-o text-warning')
							.addClass('fa-check-square-o text-success');
						//buttonReceber.hide();
					} else {
						$('[data-role=' + codigoTitulo + '] span')
							.removeClass('label-success')
							.addClass('label-warning')
							.html(e);
						
						$('[data-codigo=' + codigoTitulo + '] i')
							.removeClass('fa-check-square-o text-success')
							.addClass('fa-square-o text-warning');
					}
				});

				response.fail(function(e) {
					console.log(e);
					alert('Erro ao receber cobrança');
				});

			});
	
	$('.search-panel .dropdown-menu').find('a').click(function(e) {
		e.preventDefault();
		var param = $(this).attr("href").replace("#","");
		var concept = $(this).text();
		$('.search-panel span#search_concept').text(concept);
		$('.input-group #search_param').val(param);
	});

});