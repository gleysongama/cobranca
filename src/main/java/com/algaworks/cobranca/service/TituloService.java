package com.algaworks.cobranca.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.algaworks.cobranca.model.StatusTitulo;
import com.algaworks.cobranca.model.Titulo;
import com.algaworks.cobranca.repository.Titulos;
import com.algaworks.cobranca.repository.filter.TituloFilter;

@Service
public class TituloService {

	@Autowired
	private Titulos titulos;

	public void salvar(Titulo t) {
		try {
			titulos.save(t);
		} catch (DataIntegrityViolationException e) {
			throw new IllegalArgumentException("Formato de data inválido");
		}
	}

	public void excluir(Long id) {
		if (titulos.exists(id)) {
			titulos.delete(id);
		}
	}

	public String receber(Long codigo) {
		Titulo titulo = titulos.findOne(codigo);
		if(titulo.getStatus().equals(StatusTitulo.RECEBIDO)) {
			titulo.setStatus(StatusTitulo.PENDENTE);
		} else {
			titulo.setStatus(StatusTitulo.RECEBIDO);
		}
		titulos.save(titulo);
		return titulo.getStatus().getDescricao();
	}
	
	public List<Titulo> pesquisar(TituloFilter filtro) {
		String descricao = filtro.getDescricao() == null ? "%" : filtro.getDescricao();
		return titulos.findByDescricaoContaining(descricao);
	}
}
