package com.algaworks.cobranca.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.algaworks.cobranca.model.StatusTitulo;
import com.algaworks.cobranca.model.Titulo;

public interface Titulos extends JpaRepository<Titulo, Long>{

	List<Titulo> findByDescricaoContaining(String descricao);
	
	List<Titulo> findByDateVencimentoContaining(Date dateVencimento);
	
	List<Titulo> findByValorContaining(BigDecimal valor);
	
	List<Titulo> findByStatusContaining(StatusTitulo status);
}
