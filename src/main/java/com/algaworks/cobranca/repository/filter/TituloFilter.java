package com.algaworks.cobranca.repository.filter;

import java.math.BigDecimal;
import java.util.Date;

import com.algaworks.cobranca.model.StatusTitulo;

public class TituloFilter {
	
	private Long codigo;
	private String descricao;
	private Date dateVencimento;
	private BigDecimal valor;
	private StatusTitulo status;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Date getDateVencimento() {
		return dateVencimento;
	}
	public void setDateVencimento(Date dateVencimento) {
		this.dateVencimento = dateVencimento;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public StatusTitulo getStatus() {
		return status;
	}
	public void setStatus(StatusTitulo status) {
		this.status = status;
	}

	
}
